
# Books

Here is the list of books that I am going to read and some comments to the books I have already read.

## Linux System Administration

A short list of books covering administering of a Linux system.

* [x] Linux Basics for Hackers &mdash; Occupytheweb; 248 pages\
A perfect Linux introduction. Covers the very basics of bash scripting and file system navigation.
* [x] Linux Bible &mdash; Christopher Negus; 901 pages\
A wonderful book on Linux. Covers topics on boot process, disk management, package manager usage (Fedora/RedHat rpm), SELinux, firewalls, server configuration, cloud computing. Perfect as one of the first books.
* [x] How Linux Works: What every superuser should know &mdash; Brian Ward; 394 pages\
The book covers many topics. Probably, topics on init systems and command line tools are the best and most useful ones. Still I think it is better to read another one first. Do NOT read the russian version &mdash; the translation uses wierd words instead of traditional terms.
* [x] Linux Command Line and Shell Scripting Bible (3rd ed.) &mdash; Richard Blum; 818 pages\
A rather complete book for novices. Around a half of the book covers topics like Linux desktops or vim/emacs hotkeys. It can arguably be a good introduction for those who had no previous experience with Linux but I believe that those topics are unrelated to bash scripting. Interaction with Bash itself was described well but the complexity of operations, useful implementation notes, standard conformance, limits, coding guides and other advanced topics were left out of the book's scope. Thus the book can be treated as an OK introduction but personally I would not call it a Bible. It must also be mentioned that a newer edition was released, where some of the aforementioned issues could have been fixed.
* [x] UNIX and Linux System Administration (5th ed.) &mdash; Evi Nemeth; 1226 pages\
Brilliant book. Feels like it covers all the topics related to system administration in a well-structured manner. It starts with the basics of a system boot process, covers general \*NIX topics like process management, access control, software management, scripting, drivers and others, then goes on to discuss systems and services built on top of \*NIX (DNS, mail, web, to name a few) with networking clarified thoroughly in advance. One of the most fascinating features is that authors intentionally avoid describing the most basic service setups like LAMP on a single server with default configuration files and focus instead on the correct application architecture with DMZ, load balancers, etc. Finally, the book explains system management at scale and related problems: virtualization, containers, configuration management tools, monitoring, security issues and many more. The book is large and a broad subject is being discussed in it, so details are omitted every now and then. The book primarily focuses on giving a bird's eye view on relevant topics, sources of detailed descriptions are offered at the end of every chapter. Occasionally examples are rather compressed and thus are hard to comprehend if the reader was not already familiar with the topic, but a second read helps a lot. The text is easy to read and is reach with notes, ideas, suggestions and side notes and even contains fancy anectodes. I would personally recommend this book to anyone who wants to understand, use or administer \*NIX systems or to develop applications that will work in \*NIX environment. C. Negus's book "Linux Bible" contains more code examples and can serve as a good complement.
* [x] Practical Linux Forensics: A guide for digital investigators &mdash; Bruce Nikkel; 403 pages\
A somewhat unique book. Pays significant attention to the files that can be found on most Linux systems and log analysis. It thoroughly "investigates" topics like file systems, encryption, boot processes, networking, desktop functionality and many others. A reader would benefit from familiarity with Linux systems a lot &mdash; the very basics of Linux are usually omitted. The book is also an up-to-date one and does not limit itself to the most "traditional" services and features. Another appealing feature of the book is a characterization of desktop systems, a topic many Linux administration books lack of.
* [x] Efficient Linux at the Command Line &mdash; Daniel J. Barrett; 248 pages\
A lovely compilation of shell tricks, that can (and should) be incorporated in everyday cmdline usage. The book shows usecases for many features and focuses on combining them efficiently. The author pays much attention to writing pipelines instead of multiline scripts.
* [x] The Linux Command Line. A complete introduction (2nd ed.) &mdash; William Shotts; 506 pages\
A really gentle introduction to the command line. It was a pleasure to find different variable expansion techniques introduced nicely - I did not see this topic covered in other books about shells. The author also insists on creating safe scripts, warns about different caveats and explains popular constructs that help you avoid common problems. I would recommend this book as a first one to dive into shell usage, or at least the one that should be read right after "Linux Basics for Hackers" by Occupytheweb.
* [x] SSH, The Secure Shell. The definitive guide (2nd ed.) &mdash; Daniel J. Barrett, Richard E. Silverman, Robert G. Byrnes; 668 pages\
The book is an excellent guide to SSH. Although it was published in 2005 and uses OpenSSH3.9 as a reference implementation, the main mechanisms and configuration options remain. Most use cases are thoroughly explained having security issues always in sight. It also teaches how to debug a connection both from the user's and server's side, which is of great value.
* [x] Linux Service Management Made Easy with systemd &mdash; Donald A. Tevault; 420 pages\
SystemD is a ubiquitous init system, so it is vital to understand how it functions. Documentation, though, lacks general design discussion: SystemD consists of many moving parts and it was not obvious how they interact. Donald fixes that: in the book he touches upon many topics, even advanced ones. He explains the directory hierarchy, SystemD entities, ways to control the OS, switch between targets, set host parameters, configure logging and networking and many more. This is the best source of information to become familiar with SystemD.
* [ ] Linux in a Nutshell (6th ed) &mdash; Robert Love; 944 pages

## System Programming

Some books about low-level development in POSIX environment. Primarily using the C language.

* [x] Advanced Programming in the UNIX Environment &mdash; W. Richard Stevens, Stephen A. Rago; 1028 pages\
Excellent reference. Covers all topics on C programming in POSIX environment. Probably, only the multiprocessing topic was covered not deep enough though there are other books that focus solely on it.
* [ ] Linux System Programming. Talking directly to the kernel and C Library &mdash; Robert Love; 456 pages
* [ ] The Linux Programming Interface &mdash; Michael Kerrisk; 1556 pages

## Linux Devices/Drivers

* [x] Understanding the Linux Kernel &mdash; Daniel P. Bovet, Marco Cesati; 944 pages\
The book was written in 2005 and discusses internals of Linux 2.6. The kernel changed significantly since then yet the main concepts remain the same. Authors describe virtual memory management, process management, VFS layer and other topics an IT engineer should be familiar with. In the first place the book is for kernel hackers and probably contains too much detail for a Linux admin, but she still can make good use of the book.
* [ ] Linux Kernel Development (3rd ed.) &mdash; Robert Love; 467 pages\
Authors of "Understanding the Linux Kernel" suggest readers should read Love's book first.
* [ ] The Linux Kernel Module Programming Guide (https://github.com/sysprog21/lkmpg) &mdash; Peter Jay Salzman; 117 pages\
A constantly-evolving open-source guide.
* [ ] Linux Device Drivers &mdash; Jonathan Corbet, Alessandro Rubini; 630 pages
* [ ] Building Embedded Linux Systems &mdash; Jon Masters; 464 pages

## C Programming

* [x] The C Programming Language &mdash; Brian W. Kernighan, Dennis M. Ritchie; 272 pages\
An eternal classic. Mustread.
* [x] Expert C Programming. Deep C secrets &mdash; Peter van der Linden; 290 pages\
The best C book I have read so far. Explains really subtle and interesting topics like C declarations, type promotions, pointers (the most complete explanation I have met so far), asm-generated code and provides with a load of nice jokes.
* [x] C in a Nutshell &mdash; Tony Prinz, Tony Crawford; 620 pages\
A wonderful reference on C, touches upon many subtleties. Covers C99.
* [x] Advanced C and C++ Compiling &mdash; Milan Stevanovic; 326 pages\
Covers many interesting issues. Provides with unnecessary long code snippets and command outputs.
* [x] Pointers in C. A hands on approach &mdash; Naveen Toppo, Hrishikesh Dewan; 161 pages\
Definitely would not recommend. Covers all the pointers topics like almost all the popular C books. IMHO not in a best way. Also coverage of 1,2,3-dimentional arrays in different chapters is redundant and confusing.
* [x] Effective C. An introduction to professional C programming &mdash; Robert C. Seacord; 274 pages\
A rather short book. Covers topics for both novices (such as using IDEs and describing project structures) and experienced programmers (uses \*NIX command line with relatively no introduction). The book aims at telling how to write safe and standard-conforming code but the rules and guides are rather sparse and the detailed descriptions of some issues are left to the CERT standard or the other author's book. Unfortunately, code examples contain subtle errors that break some of the rules described earlier. Those errors could have been introduced unintentionally or to shorten the examples but in both cases they stand out due to the frequent notes about security and UB. IMHO, the book tries to cover two incompatible topics: a gentle introduction to programming with the C language and relatively advanced coding issues, and thus I cannot tell who is the intended reader of the book. Anyways, the overall impression is positive. The author also pays much attention to integer arithmetics and points out many corner cases, which are often left untouched.
* [x] 21st Century C &mdash; Ben Klemens; 298 pages\
A controversial book. The author has an agenda that focuses primarily on effectiveness, new language features and its ecosystem, which is awesome. The book encourages use of stable and widespread libraries to avoid inventing the wheel, demonstrates features of the newer standards. At the same time the whole philosophy of the book is dictated by the author's occupation and education: he is a scientist, not a programmer. Most of the pieces of advice in the book are only related to the programs that run on modern software for a limited time like simulations. Security issues, error checking, reliability, extreme effectiveness are not a big issue for such a software and for the author as well. It is surprising that the author touches upon software distribution problems, testing or debugging and say almost nothing about optimizations, but C calls for such a topic. Unfortunately, the book is too short to cover environment thoroughly, so it only gives a general overview and a list of pieces of advice. Code examples are sometimes inconsistent, even regarding the codestyle, and several author's opinions are considered a bad practice. Scattered notes also do not give the whole picture and only show useful tricks. The books reminds a printed version of a blog (and it is, in some part). I would recommend it as a source of inspiration of how to use C11 and not as a reference. The reader should be experienced enough to filter out harmful ideas and find brilliant ones by themselves.
* [x] C Interfaces and Implementations &mdash; David R. Hanson; 533 pages\
Mustread, especially for those who did not develop any general-purpose libraries and interfaces before. Presumably covers most of the techniques used to implement data structures in C, or at least the most popular ones. Unfortunately, it uses literal programming approach to describe the code, which is hard to understand with an electronic edition of the book, but it is still an invaluable source of information. Chapters related to implementation of custom allocators, exceptions and user-space threads are the ones that are rarely covered in books and are definitely worth reading.
* [ ] Modern C &mdash; Jens Gustedt; 408 pages\
Covers the very recent C standards.
* [ ] Linkers and Loaders &mdash; John R. Levine; 299 pages\
Probably a bit outdated one but still actual.
* [ ] Computer Systems. A programmer's perspective &mdash; Bryant O'Hallaron; 1078 pages\
A comprehensive overview of low-level C programming.
* [ ] Programming with POSIX Threads &mdash; David R. Butenhof; 398 pages
* [ ] Applied Cryptography: Protocols, Algorithms and Source Code in C (2nd ed.) &mdash; Bruce Schneier; 784 pages

## C++

A lot of books are written about C\+\+ and many of them are considered of an excellent quality. Only a little subset of the books I am going to read or already read is presented here. Also there are many online resources on C\+\+ worth visiting.

* [x] C++ Primer Plus (6th ed.) &mdash; Stephen Prata; 1244 pages\
Covers many C++ features from the very beginning, adopting the down-top approach. Does not go into much detail of C\+\+11 and STL. A very good option for the first C\+\+ book.
* [x] The C++ Programming Language (4th ed.) &mdash; Bjarne Stroustrup; 1366 pages\
A comprehensive high-level guide to C\+\+. Does not go into too mush detail and encourages a reader to use the standard library and avoid the C code as much as possible. Promotes top-down study process: going from high-level code to the internals.
* [ ] Effective C++ (3rd ed.) &mdash; Scott Meyers; 321 pages\
Explains many really useful items without even touching C\+\+11.
* [ ] More Effective C++ &mdash; Scott Meyers; 337 pages
* [ ] Effective STL &mdash; Scott Meyers; 288 pages
* [ ] Effective Modern C++ &mdash; Scott Meyers; 334 pages\
Covers C\+\+11 and C\+\+14 language standards.
* [ ] C++ Concurrency in Action (2nd ed.) &mdash; Anthony Williams; 592 pages\
One of the most complete books on concurrency in C\+\+. Skimming through this book created an impression like the book is a sort of more "concise" version of "The Art of Multiprocessing Programming" by Maurice Herlihy but the former provides with more code examples and less theory.

## Python

There are a lot of books that are covering basic Python features and trying to explain what the programming is simultaneously. I have struggled to find more fundamental books on Python. So here is the list.

* [x] Python Tutorial &mdash; Guido van Rossum; 155 pages\
One of the most complete introductions to Python for the very beginners. A good one as the first book, also contains several interesting code snippets.
* [x] Python in a Nutshell (3rd ed.) &mdash; Alex Martelly; 767 pages\
The best reference book. Covers many in-depth topics, the best second book on Python.
* [x] Effective Python. 90 specific ways to write better Python &mdash; Brett Slatkin; 472 pages\
The book requires some knowledge and experience in writing Python to comprehend covered topics. The most interesting ones are devoted to writing "pythonic" code, performance and multithreading. Written like an "Effective C++" book series by Scott Meyers.
* [x] Python Tricks. A buffet of awesome Python features &mdash; Dan Bader; 299 pages\
Delivers many interesting topics but after the "Effective Python" and "Python in a Nutshell" books there all look quite familiar.
* [x] Think Python. How to think like a computer scientist &mdash; Allen Downey; 300 pages\
The book focuses on a programming process and terms rather than Python. Probably a good one if you have not programmed before but cannot be treated even as a first book on Python if you are looking for Python features and best practices.
* [x] Using Asyncio in Python. Understanding Python's asynchronous programming features &mdash; Caleb Hattingh; 166 pages\
Asynchronous programming is an important but difficult concept of modern programming. The official documentation looks sophisticated, provides with a mix of high-level and low-level details and lacks a gentle introduction - and these are the problems the book solves. The author also gives a bunch of links to important talks and other sources that help you develop a better understanding of asynchio concepts. The only controversial topic is the author's desire to make programming with threads look like an impossible task. It is difficult but it is definitely not as hard as shown in the book, though requires significant effort to master it.
* [x] CPython Internals &mdash; Anthony Shaw; 385 pages
Not every Python developer would need to read this book. It is a unique book written for engineers aiming at hacking the CPython interpreter. It explains the C code interpreter written in, the project layout and intimate issues. Unfortunately, the author did not pay much attention to optimizations in CPython that would be beneficial to know for Python users.
* [x] Serious Python. Black-belt advice on deployment, scalability, testing and more &mdash; Julien Danjou; 242 pages\
The author assumes a reader is already familiar with Python and has some experience using many of the features the language provides. The book indeed covers a lot of topics that are skipped in more fundamental books. It explains how to customize the import system, lists the most useful modules from the standard library, dives into date and time handling in Python, gives details of OOP and AST implementation, covers optimization topics and many more. I am sure any Python user will find something interesting in this book.
* [ ] High Performance Python &mdash; Micha Gorelick, Ian Ozsvald; 468 pages\
Focuses on code profiling.
* [ ] Fluent Python. Clear, concise, and effective programming &mdash; Luciano Ramalho; 766 pages\
Contains many truly pythonic code examples. Uses third-party libraries for examples so the book might be hard to understand for those who do not use Python on everyday basis.
* [ ] Python Cookbook. Recipes for mastering Python 3 (3rd ed.) &mdash; David Beazley, Brian K. Johnes; 708 pages\
Provides with excessive amount of code snippets. Probably may be a bit outdated.

## GoLang

GoLang is gaining popularity right now and is a relatively new language. That is why it is not clear now which books give truly good pieces of advice and cover important topics.

* [x] The Go Programming Language &mdash; Alan A. A. Donovan, Brian W. Kernighan; 399 pages\
A nice introduction to the GoLang. Explains why many things were done in a way they were done. One of the most notable notes is about fixing the C design errors and explaining which purpose does GoLang serve.
* [ ] Learning Go: An ideomatic approach to real-world Go programming &mdash; Jon Bodner; 374 pages\
Covers generics, adopted in golang1.18
* [ ] Mastering Go (2nd ed.) &mdash; Mihalis Tsoukalos; 784 pages\
Probably covers the GoLang internals.

## DevOps/SRE

* [x] Using Docker &mdash; Adrian Mouat; 355 pages\
Introduces the Docker technology nicely. Apart from covering the basic docker operations and philosophy also covers many related topics such as testing, auditing, orchestration, networking, etc. Due to the active development the book might be a bit outdated (published in 2016).
* [x] Ansible: Up and Running (2nd ed.) &mdash; Lorin Hochstein, René Moser; 429 pages\
The authors did a great job on giving a fast and precise working introduction to configuration management. They also covered recently developed tools. 3rd ed. is coming soon and will likely bring many updates.
* [x] Prometheus: Up and Running &mdash; Brian Brazil; 384 pages\
The book explains some of the issues, related to metric-based monitoring, and explains how Prometheus addresses them. Instrumenting the source code, writing and using common exporters, configuring alerting and using PromQL are all thoroughly discussed. Although the author uses Prometheus version 2.2.1, all the topics still apply to the newer versions 2.x.x.
* [x] NGINX Cookbook. Advanced recipes for high-performance load balancing &mdash; Derek DeJonghe; 207 pages\
As a cookbook, this book assumes a reader is already familiar with basic NGINX configuration and use cases, and does not delve into the description of syntax or module architecture. The author teaches how to setup a server in the desired way, deal correctly with the most common configuration options and push the service to the limits.
* [x] Kafka: The Definitive Guide (2nd ed.) &mdash; Gwen Shapira, Todd Palino, Rajini Sivaram, Krit Petty; 486 pages\
Covers installation, architecture, deployment and most use cases of Kafka. Example clients are also provided. Interestingly, the authors give introduction to large-scale system architectures as well before diving into nits and bolts of proposed techniques and solutions. Authors also did great job explaining internals of Kafka, that enable the discussed features. It would be also great to see a list of dedicated roles in a cluster, since Kafka introduces a number of them, and it easy to confuse them at first. For now a reader must search for the roles in the internet or across the book by themselves.
* [x] Terraform: Up and Running. Writing Infrastructure as Code (3nd ed.) &mdash; Yevgeniy Brikman; 460 pages\
The author wanted to explain the ideas behind IaC and the way Terraform should be used, not only cover its functionality. He pays much attention to explaining the benefits of IaC and why it should be used. He also points out many project management issues, related to IT infrastructure. From the technical point of view the author does a great job explaining why each feature of Terraform was added, as well as discussing basic and advanced usage, testing and security. The book is definitely worth reading.
* [x] Kubernetes: Up and Running (2nd ed.) &mdash; Brendan Burns, Joe Beda, Kelsey Hightower; 277 pages\
A book is written by authors of Kubernetes and provides with a user's perspective of k8s. Authors did great job explaining the idea behind k8s objects and their relationships. They also pay much attention to requirements, imposed on modern applications, such as fault tolerance, release speed, reliability and others. Authors introduce k8s concepts and objects only after having touched upon a reason that gave birth to the object, which is cool -- they never leave a reader with a question "why?". The book covers basic and most important objects of k8s, providing with a solid background without consuming too many pages.
* [x] Docker: Up and Running (3rd ed.) &mdash; Sean P. Kane; 419 pages\
The most complete and valuable source of information about Docker encountered so far. Docker evolves rapidly and it is not easy to catch up with all the tools created around and yet this book does excellent job explaining both core concepts of containers and ways to get as much as possible from adopting them. The authors give Docker basics first, then dive into debugging tools and strategies and even focus on files maintained by the daemon, rendering the whole container lifecycle crystal-clear. They also pay attention to the modular structure of docker, which allows for better understanding of the mechanics behind the commands and purpose of different tools and layers involved. Security issues and deployment best practices are also considered in the book.
* [x] Learning OpenTelemetry. Setting Up and Operating a Modern Observability System &mdash; Ted Young, Austin Parker; 170 pages\
The book is worth reading not only as an introduction to OTel but also as a general talk about observability. It covers the modern state of observability and its foundations thoroughly, steps through existing problems and shows how OTel addresses them. OTel philosophy is clearly explained without going deep into the implementation details - links to GitHub repo and official documentation is given instead. This book is a marvelous and indispensable complement to the OTel documentation.
* [x] Managing Kubernetes. Operating Kubernetes Clusters In The Real World &mdash; Brendan Burns, Craig Tracey; 187 pages\
Written in 2017, the book remains relevant nowdays, in 2024. It explains core concepts of Kubernetes and ways to extend the system. Authors describe how the components function and outline the consequences of their misbehavior. Control plane structure, k8s installation and administration, request authn/authz and networking are all topics covered in the book. The book makes it quite clear how Kubernetes works and what stands behind the Kubernetes API.
* [x] Podman in Action. Secure, Rootless Containers for Kubernetes, Microservices, and More &mdash; Daniel Walsh; 314 pages\
This book should be read by any engineer working with Docker. This book not only describes the Podman command line utility but also the rationale behind its creation: Daniel explains why did he decide to build a new set of utilities that would do the same as existing ones. He points out weak points of Docker architecture, the most notable one being security. Client-server model, lack of authentication, lack of SystemD support, root access granted to all Docker users and a couple more are all points of concern. Podman tries to do the same things but in a better and more secure way. Podman also supports Pods and understands k8s manifest files making it easier to debug k8s applications locally. Despite the feature-reach nature of the tool it maintains strict compatibility with Docker, so it can be used as a drop-in replacement for docker tool. A significant part of the book is also devoted to the Linux features Podman exploits, like namespaces and SELinux. In a nutshell, the book is versatile and is really joy to read.
* [ ] Learning Helm. Managing Apps on Kubernetes &mdash; Matt Butcher, Matt Farina, Josh Dolitsky; 215 pages
* [ ] Learn Kubernetes in a Month of Lunches &mdash; Elton Stoneman; 676 pages

## Databases, SQL

* [x] Getting Started with SQL &mdash; Thomas Nield; 133 pages\
A short and nice introduction to SQL.
* [x] Learning SQL. Generate, Manipulate, and Retrieve Data &mdash; Alan Bealieu; 380 pages\
A good one. Covers many topics. Looks perfect as the second book after the previous one in this list.
* [ ] The art of PostgreSQL &mdash; Dimitri Fontaine; 457 pages
* [ ] SQL in a Nutshell &mdash; Kevin E. Kline; 593 pages\
Looks like a complete reference...
* [ ] Practical Issues in  Database Management &mdash; Fabian Pascal; 272 pages\
Covers important things in creating and managing relational databases.

## Databases, NoSQL

* [x] Elasticsearch in Action (2nd ed.) &mdash; Madhusudhan Konda; 593 pages\
The book is a complete and comprehensive introduction to Elasticsearch. The author follows hands-on approach and solves given problems with Elasticsearch, while discussing new features. Basic and advanced searching techniques, index creation and settings and database internals are discussed as well as cluster administering and troubleshooting. The book is the one that should be used as the first one on Elasticsearch.

## Programming Skills

Books on topics relevant to any programming language.

* [x] The Practice of Programming &mdash; Brian W. Kernighan, Rob Pike; 272 pages\
Touches upon many issues such as naming, design, testing, etc. Provides with a concise list of advises at the end.
* [ ] Design Patterns. Elements of Reusable Object-Oriented Software &mdash; The Gang of Four; 431 pages

## Version Control Systems

* [x] Pro Git &mdash; Scott Chacon, Ben Straub; 542 pages\
A de-facto standard introduction to Git. Translated to many languages, can be read online. Covers the most important porcelain git commands, used in an everyday work, as well as the plumbing commands. The git internals are described as well. The book is a good start for those who didn't use this SCM before. Roughly half of the book is devoted to more subtle topics such as migrating to/from other SCMs, writing custom commands, using libgit2, etc.
* [ ] Mercurial: The Definitive Guide &mdash; Bryan O'Sullivan; 183 pages

## Hack

A list of books giving a little insight into information security field. Most of them cannot be treated as complete reference books but the topics covered there are really entertaining and informative. Great for broadening horizons in IT.

* [x] Hacking. The art of exploitation (2nd ed.) &mdash; Jon Erickson; 488 pages\
Explains many conventional attacks on example. The author develops the exploits and provides with code examples. Those code examples cannot be used in real life since the security measures already prevent them from running but the explanations are great.
* [x] Real-World Bug Hunting &mdash; Peter Yavorski; 264 pages\
Just several amusing examples of web vulnerabilities.
* [x] Penetration testing: A Hands-On Introduction to Hacking &mdash; Georgia Weidman; 531 pages\
This book really does what it claims to do. Georgia Weidman guides a reader through many known attacks and really allows to practice them. Every known issue is explained and then a tool is used to exploit the discussed vulnerability. Georgia even shows how to develop your own exploits and reverse-engineer programs without having their source code at hand. The book serves only educational purposes: vulnerabilities in the book can be considered "canonical" and can hardly be met in the wild now. Default behavior of modern software makes every discussed vulnerability impossible or at least extremely hard to exploit. Still it is a very educative and interesting book that should be read even by developers to understand the ways their software gets attacked.
* [ ] The Art of Memory Forensics &mdash; Michael Hale Ligh; 914 pages\
Looks like a comprehensive guide to forensics. One of the most "serious" books in this list.
* [ ] PowerShell for Penetration Testing &mdash; Dr. Andrew Blyth; 298 pages

## Security

* [x] SSL and TLS. Theory and practice (2nd ed.) &mdash; Rolf Opplinger; 301 pages\
The book covers all versions of SSL and TLS standards. Message format was explained in detail, as well as security implications and evolution process. PKI and x509 certificate format were discussed thoroughly as well. The book provides with a profound backgroud in the topic.
* [x] The JWT Handbook &mdash; Sebastian E. Peyrott, Auth0 Inc.; 120 pages\
JWTs and related JWx-things are explained nicely in the book. Prior knowledge of JS will be of a great help, some examples really assume confidence with JS. One third of a book explains cryptography  as well, so it can also be considered as a nice introduction to cryptography algorithms implementations. Still it can hardly be considered a book to read first about JWTs, sometimes the text is rather hard to follow.
* [x] The OpenID Connect Handbook &mdash; Bruno Krebs, Auth0 Inc.; 66 pages\
This is a book worth reading. Not only it explains OAuth2 and OpenID Connect basics, but also provides thorough introduction to authentication/authorization topic, followed by brilliant examples. Code examples are given in JS and contain detailed and valuable comments.
* [x] OpenSSL Cookbook (3rd ed.) &mdash; Ivan Ristic; 84 pages\
A very useful book. Examples cover many real-world cases and explain most common operations on certificates. Caveats are also discussed thoroughly.
* [x] Linux Firewalls: Enhancing Security with nftables and Beyond (4th ed.) &mdash; Steve Suehring; 425 pages\
The book indeed explains the foundations of a firewall setup in Linux. It starts with discussion of necessary networking topics, than explains netfilter architecture and `iptables` and `nftables` command line interfaces to netfilter. The author than builds an example firewall host from the ground up and shows ways to improve the solution. A significant part of the book covers related security issues like network traffic monitoring, filesystem integrity checks, common types of attacks and useful tools. The book does great job explaining the theory of firewalls and yet it feels like some topics could have been explained in more detail. For example, netfilter architecture and relationships between chains of different tables were not clearly discussed, so it took a while to find more information online before moving on. Despite this fact, the book is definitely worth reading.
* [x] Practical Vulnerability Management &mdash; Andrew Magnusson; 196 pages\
Written by an experienced security engineer, this book is rather vapid. The final goal of the book is to build a decent security posture from the ground up using only free and available tools, so you will not find any recommendations regarding integrations with existing systems, best practices or sophisticated tools. A lot of reasoning is devoted to the problems a security engineer may encounter, human interactions being the most difficult one, so the author gives some arguments that you can use to communicate with managers and other teams. Ideas in the book are very basic that they would prevent a reader from only making the most severe and stupid mistakes. The practical part of the book is also controversial: the author builds a security system based on `nmap`, `OpenVAS` and MongoDB having scripts run in `cron` weekly. The resulting system feels complex enough to be designed in a more resilient and user-friendly way, with DRP and maintenance processes developed as well - and yet it just consists of awkward scripts that even have typos. In short, the practical part full of weird decisions: solution is rather fragile to be operated by a newbie and is too primitive for a more experienced administrator. As a whole, I have found the theoretic part of the book more useful than the practical one. Although there were no harmful ideas, the book provides with just a few applicable ones that are scattered across the whole text.
* [ ] Bulletproof TLS and PKI &mdash; Ivan Ristic; 481 pages
* [ ] OAuth2 in action &mdash; Justin Richer, Antonio Sanso; 362 pages
* [ ] Password authentication for web and mobile apps &mdash; Dmitry Chestnykh; 130 pages
* [ ] SELinux System Administration (3rd ed.) &mdash; Sven Vermeulen; 459 pages
* [ ] Mastering Linux Security and Hardening (3rd ed.) &mdash; Donald A. Tevault; 619 pages
* [ ] Real-World Cryptography &mdash; David Wong; 400 pages

## Web Programming

Web programming is a rapidly developing field, so it requires much effort to stay on the bleeding edge. At the same time, it might be of great value to understand the basic principles. Web is also closely related to SRE topic, so a SRE should be aware of main web concepts as well.

* [x] PHP, MySQL, JavaScript & HTML5 for Dummies &mdash; Steve Suehring, Janet Valade; 724 pages\
An incredibly gentle introduction to web programming. A very high-level and light description of LAMP idiom and a tutorial how write a couple of HTML pages. The informative part takes roughly a half of the book: chapters devoted to Apache and MySQL installation are almost identical but each one takes much place.
* [x] Don't make me think, revisited: A common sense approach to Web and Mobile usability &mdash; Steve Krug; 214 pages\
The author briefly explains how a Web page should look and feel without going into technical details. He pays much attention to the usability testing &mdash; the idea, the methodology, etc. The book is very well organized, contains many lists that can be used as-is, almost like checklists. References to many other authoritative sources included.
* [x] Eloquent JavaScript. A modern introduction to programming &mdash; Marijn Haverbeke; 476 pages\
The book teaches programming from the very beginning. At the same time it focuses on many useful JS features, that might be interesting even for somewhat experienced developers. It also gives a lot of background on web technologies. The author presents asynchronous programming as a natural evolution of callback-based paradigm, which is great and thus avoids any mind-blowing explanations. Chapters about node.js also contribute to the value of the book, since described techniques can be met in other programming languages, such as Golang.
* [x] Learning React. Modern Patterns for Developing React Apps (2nd ed.) &mdash; Alex Banks, Eve Porcello; 310 pages\
Wonderful introduction to React framework, authors explain the features and ideas behind React by explaining the history of their appearence ang giving vivid examples. Unfortunately, printed books about web technologies tend to become obsolete too soon, so even GitHub code is compiled with warning in 2023.

## Computer Science

Classical Computer Science books.

* [ ] Introduction to Algorithms (3rd ed.) &mdash; Thomas H. Cormen; 1313 pages\
Sort of a classical book on algorithms.
* [ ] Algorithms (4th ed.) &mdash; Robert Sedgewick; 969 pages\
A comprehensive book on algorithms. Uses Java.
* [ ] The Art of Computer Programming, Volumes 1-4 &mdash; Donald Knuth\
The bible of computer programming.
* [ ] The Art of Multiprocessing Programming (2nd ed.) &mdash; Maurice Herlihy, Nir Shavit; 562 pages\
The best theoretical introduction to the parallel programming I have met so far.
* [ ] Structure and Interpretation of Computer Programs (2nd ed.) &mdash; Hal Abelson; 658 pages

## OS Structure/Internals 

* [x] Modern Operating Systems &mdash; Andrew S. Tanenbaum; 1136 pages\
A comprehensive and complete introduction to OS. Adopts primarily theoretical approach.
* [ ] Operating Systems. Design and implementation (3rd ed.) &mdash; Andrew S. Tanenbaum, Albert S. Woodhull; 1088 pages\
Almost the same as the "Modern Operating Systems" book but ships with MINIX source code and focuses on the OS implementation.
* [ ] The Design of the UNIX Operating System &mdash; Maurice J. Bach; 484 pages\
Some technical info on signal processing might be useful here.
* [ ] Linux Kernel Programming (2nd ed.) &mdash; Kaiwan N. Billimoria; 827 pages

## Uncategorized

Probably, later some of the books will form their own section but for now they are placed here together.

* [x] Introducing Regular Expressions &mdash; Michael Fitzgerald; 154 pages\
A gentle introduction to regexes.
* [x] Parallel Programming with MPI &mdash; Peter S. Pacheco; 444 pages\
A nice overview of MPI library interface and usage. Code examples look a bit outdated.
* [x] Mathematical Elements for Computer Graphics (2nd ed.) &mdash; David F. Rogers, J. Alan Adams; 512 pages\
A fundamental book that covers different maths such as linear algebra and splines.
* [x] Blockchain Blueprint for a New Economy &mdash; Melanie Swan; 149 pages\
The book does not touch upon technical side of cryptocurrencies and blockchain. Instead it focuses on how the technology can be applied in the future. The author gathers and presents the opinions of many blockchain visionaries and tries to convince a reader that it is a giant step for the whole humanity. At the time of reading (2022) the book's age is roughtly 7 years, and the blockchain did not receive that amount of attention.
* [x] LDAP Programming, Management and Integration &mdash; Clayton Donley; 352 pages\
Feels like LDAP concept was covered nicely. The book describes common pitfalls, best practices when deploying LDAP and ways to integrate apps with existing LDAP servers. At the same time it feels rather old. It was written in 2003, so the author expected that SOAP will take over and code examples are given in Perl and Java2. Most part of the book was filled with useful code examples, though now they might seem a bit outdated.
* [x] GAWK: Effective AWK Programming (5.3 ed., oct. 2023) &mdash; Arnold D. Robbins; 605 pages\
The book covers both AWK basics and GNU extensions. The author does great job giving an intro to AWK and programming in general and explains the reasons to create the GNU version of awk tool. Examples in the book look a bit different from typical use cases of AWK and are mostly long programs, whilst the language was designed primarily for writing ad-hoc scripts. The book also pays attention to advanced topics like profiling the code and writing extensions.
* [x] The AWK Programming Language (2nd ed.) &mdash; Alfred V. Aho, Brian W. Kernighan, Peter J. Weinberger; 231 pages\
The books is structured like any other Kernighan's book about languages. First of all, it explains how to write simple but useful AWK oneliners without diving into syntax details, so a reader can start using awk as soon as possible. The author tells about the AWK's initial purpose and its development and thus makes design decisions clear. This book is an excellent complement to "GAWK: Effective AWK Programming" since it focuses on solving tasks AWK was designed for.
* [x] Learning HTTP/2. A practical guide for beginners &mdash; Stephen Ludin, Javier Garza; 156 pages\
The book explains the rationale behind the HTTP/2 protocol and its structure. Authors give examples of HTTP/1 applications, shortcomings of HTTP/1 and how these inefficiencies were addressed before the HTTP/2 was created. Based on the developed understanding of HTTP/1 tricks authors make design decisions clearer and show how the new HTTP/2 protocol simplifies apps and decreases latency.
* [x] DNS and BIND (5th ed.) &mdash; Cricket Liu, Paul Albitz; 642 pages\
This book may feel outdated but in reality the core concepts of DNS stay the same. Significant part of the book is devoted to setting up a DNS server: it explains the preferred architecture, interaction with firewalls, master-slave communication issues, subdomain delegation rules. Authors also pay attention to client-side configuration. Yet it feels like many topics were subject to change at the time of writing, some promising features finally became obsolete or are still of very limited use. It is still important to consult up-to-date sources when studying new concepts from the book. The basics of DNS lookup process, client-server interaction and request processing did not change drastically and are explained perfectly in the book.
* [x] Java. A Beginner’s Guide (9th ed.) &mdash; Herbert Schildt; 753 pages\
This is probably one of the most famous books about Java programming language. Herbert Schildt introduces Java to a reader in a slow paced manner, with lots of details explained. Author also assumes a reader has little to no experience in low-level programming, so he pays much attention to some basic concepts like integer representation and alike. These notes are still worth skimming through even for experienced programmers. The book has structure very similar to Stephen Prata's "Primer Plus" and touches upon many topics: Java fundamentals, release cycles, inheritance support, packages and modules, I/O, exceptions, interfaces, multithreading, generics and many more. Surrounding subjects like project structure, build process, JVM implementation, performance measurements are not discussed, the author concentrates solely on the language. The only feature of the book that remains questionable is code examples: for some reason portion of examples contains code with smell. Such code compiles and in most cases works well, though may behave unexpectedly, and the author does not draw reader's attention to such issues. In all other parts the book is a solid introduction to Java and is worth reading.
* [x] bash Idioms. Write Powerful, Flexible, Readable Shell Scripts &mdash; Carl Albing, JP Vossen; 170 pages\
The book contains a fair amount of idiomatic Bash code samples. Authors did a great job finding and explaining common and widespread idioms. They also point out that most of advice given in the book are related to Bash and are not really portable - and yet the book is worth reading.
* [ ] Managing Projects with GNU Make &mdash; Robert Mecklenburg; 302 pages\
The first chapter covers most of the topics needed to create OK Makefiles.
* [ ] Autotools. A practitioner's guide to GNU Autoconf, Automake, and Libtool &mdash; John Calcote; 586 pages\
Useful to understand the process of building a package.
* [ ] Designing Data-Intensive Applications: the big ideas behind reliable, scalable, and maintainable systems &mdash; Martin Kleppmann; 613 pages\
A classical book.
* [ ] Programming Pearls &mdash; Jon Bentley; 195 pages\
Tells the stories of many computational problems and their effective solutions. Teaches the way to think of a problem and how to come up with a better solution.
* [ ] More Programming Pearls &mdash; Jon Bentley; 204 pages\
The successor of "Programming Pearls".
* [ ] Compilers. Principles, techniques, and tools (2nd ed.) &mdash; Alfred V. Aho; 1035 pages\
The bible of compiler-writing. Also is referred to as a "Dragon Book".
* [ ] Computer Architecture. A quantitative approach (5th ed.) &mdash; John L. Hennesey, David A. Patterson; 856 pages\
Describes the modern computer architecture. The topics help to write better code ultimately.
* [ ] Hacker's delight (2nd ed.) &mdash; Henry S. Warren, Jr.; 512 pages\
Covers incredible maths used in compiler optimizers.

